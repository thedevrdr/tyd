pub fn starts_with(line: &str, words: &[&str]) -> bool {
    words.iter().any(|e| line.starts_with(e))
}

pub fn count_ends_with(mut line: &str, suffix: &[&str]) -> usize {
    if !ends_with(line, suffix) {
        return 0;
    }

    let mut r = true;
    let mut c = 0;

    while r {
        let res = suffix
            .iter()
            .enumerate()
            .filter_map(|(i, e)| {
                if line.ends_with(e) {
                    line = &line[..line.len() - e.len()];
                    return Some((i, true));
                }

                None
            })
            .count();
        c += res;
        r = res > 0;
    }

    c
}

pub fn count_starts_with(mut line: &str, suffix: &[&str]) -> usize {
    if !ends_with(line, suffix) {
        return 0;
    }

    let mut r = true;
    let mut c = 0;

    while r {
        let res = suffix
            .iter()
            .enumerate()
            .filter_map(|(i, e)| {
                if line.starts_with(e) {
                    line = &line[..line.len() - e.len()];
                    return Some((i, true));
                }

                None
            })
            .count();
        c += res;
        r = res > 0;
    }

    c
}

pub fn strip_starts_with(line: &str, words: &[&str]) -> usize {
    words.iter().any(|e| line.starts_with(e));
    0
}

pub fn ends_with(line: &str, words: &[&str]) -> bool {
    words.iter().any(|e| line.ends_with(e))
}
